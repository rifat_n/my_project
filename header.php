<?php

include 'system/prolog.php';
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/assets/styles/style.css">
    <title>ИПР</title>
</head>
<body>

<header>
    <div class="container">
        <a href="/">Главная</a>
        <a href="/news/">Статьи</a>
        <?php if ($isAuth):?>
            <span class="logout">Выйти</span>
        <?php else:?>
            <a href="/auth/">Авторизация</a>
            <a href="/reg/">Регистрация</a>
        <?php endif;?>
    </div>
</header>