<?php
include_once '../header.php';

if (isset($_SESSION['user_id'])) {
    header("Location: /");
    exit();
}

$authErrors = [];
$success = false;

if ($_POST) {
    $checkAuth = $user->checkAuth($_POST);
    if ($checkAuth) {
        $success = true;
    }
}

if (!$success) :?>
    <div class="main container">
        <form class="reg__form" method="POST"  action="/auth/">
            <input type="text" required placeholder="Имя" name="name" value="<?=$_POST['name'] ?? ''?>">
            <input type="password" required placeholder="Пароль" name="pass">
            <input type="submit" value="Войти">
            <?php if ($_POST):?>
                <div class="errors">
                    <div class="error">
                        Неверный логин или пароль!
                    </div>
                </div>
            <?php endif;?>
        </form>
    </div>
<?php else:
    $session = $user->setSession($checkAuth->id);
    header("Location: /");
    exit();
endif;

include_once '../footer.php';