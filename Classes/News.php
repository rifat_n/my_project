<?php

require_once('NewsStore.php');

class News
{
    private $news_store;

    public function __construct(DataBase $db)
    {
        $this->news_store = new NewsStore($db);
    }

    public function getNews($page)
    {
        return $this->news_store->getNews($page);
    }

    public function insertNew($request, $id)
    {
        return $this->news_store->insertNew($request, $id);
    }

    public function getPost($id)
    {
        return $this->news_store->getPost($id);
    }
}