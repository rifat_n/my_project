<?php


class UserStore
{
    public $db;

    public function __construct(DataBase $db)
    {
        $this->db = $db;
    }

    public function getUserData($id)
    {
        $query = "SELECT * FROM " . U_TABLE . " WHERE `id` = $id";
        return $this->db->selectRow($query);
    }

    public function checkErrors($request)
    {
        $name = $request['name'];
        $mail = $request['email'];
        $pass1 = $request['pass'];
        $pass2 = $request['pass2'];
        $errors = [];

        if ($pass1 != $pass2) {
            $errors[] = 'Введенные пароли не совпадают!';
        } elseif ($pass1 == '') {
            $errors[] = 'Введите пароли!';
        }

        $query = "SELECT * FROM " . U_TABLE . " WHERE `login` = '$name' OR `email` = '$mail'";
        $data = $this->db->selectRow($query);
        if ($data['login'] == $name) {
            $errors[] = 'Пользователь с таким именем уже существует!';
        }
        if ($data['email'] == $mail) {
            $errors[] = 'Пользователь с таким email уже существует!';
        }
        return $errors;
    }

    public function doRegister ($request)
    {
        $name = $request['name'];
        $mail = $request['email'];
        $pass = $request['pass'];
        $date = date('Y-m-d H:i:s');
        $query = "INSERT " . U_TABLE . " SET `login` = '$name', `email` = '$mail', `password` = $pass, 
            `created_at` = '$date', `updated_at` = '$date'";

        return $this->db->query($query);
    }

    public function setSession ($id)
    {
        session_start(['cookie_lifetime' => 3600]);
        $_SESSION['user_id'] = $id;
    }

    public function checkAuth($request)
    {
        $name = $request['name'];
        $pass = $request['pass'];
        $query = "SELECT * FROM " . U_TABLE . " WHERE `login` = '$name' AND `password` = '$pass'";

        return (object)$this->db->selectRow($query);
    }
}