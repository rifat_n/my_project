<?php


class NewsStore
{
    public $db;

    public function __construct(DataBase $db)
    {
        $this->db = $db;
    }

    public function getNews($page)
    {
        $limit = 3;
        $offset = ceil($limit * (intval($page) - 1));
        $query = "SELECT * FROM " . N_TABLE . " LIMIT $limit OFFSET $offset";
        $data['posts'] = $this->db->select($query);
        $query = "SELECT * FROM " . N_TABLE;
        $data['page_count'] = ceil(count($this->db->select($query)) / 3);
        return $data;
    }

    public function insertNew($request, $id)
    {
        $title = $request['title'];
        $text = $request['text'];
        $date = date('Y-m-d H:i:s');
        $userId = intval($id);
        $query = "INSERT " . N_TABLE . " SET `user_id` = $userId, `title` = '$title', `post` = '$text', 
            `created_at` = '$date'";

        return $this->db->query($query);
    }

    public function getPost($id)
    {
        $query = "SELECT * FROM " . N_TABLE . " WHERE `id` = $id";

        return (object)$this->db->selectRow($query);
    }
}