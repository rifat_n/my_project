<?php

class DataBase
{
    private $server = CONFIG['dataBase']['server'];
    private $login = CONFIG['dataBase']['login'];
    private $password = CONFIG['dataBase']['password'];
    private $db_name = CONFIG['dataBase']['db_name'];

    public static $db = null; // Единственный экземпляр класса
    public $mysqli; // Идентификатор соединения

    public static function getDB()
    {
        if (self::$db == null) self::$db = new DataBase();
        return self::$db;
    }

    private function __construct()
    {
        try {
            $this->mysqli = new mysqli($this->server, $this->login, $this->password, $this->db_name);
            $this->mysqli->set_charset(CONFIG['dataBase']['charset']);
        }
        catch (Exception $ex) {
            echo "Произошла ошибка - " . $ex->getMessage();
        }
    }

    /* SELECT-метод, возвращающий таблицу результатов */
    public function select($query)
    {
        $result_set = $this->mysqli->query($query);
        if (!$result_set) {
            return false;
        } else {
            return $this->resultSetToArray($result_set);
        }
    }

    /* SELECT-метод, возвращающий одну строку с результатом */
    public function selectRow($query)
    {
        $result_set = $this->mysqli->query($query);
        if ($result_set->num_rows != 1) {
            return false;
        } else {
            return $result_set->fetch_assoc();
        }
    }

    /* НЕ-SELECT методы. Если запрос INSERT, то возвращается id последней вставленной записи */
    public function query($query)
    {
        $success = $this->mysqli->query($query);
        if ($success) {
            if ($this->mysqli->insert_id === 0) {
                return true;
            } else {
                return $this->mysqli->insert_id;
            }
        } else {
            return false;
        }
    }

    /* Преобразование result_set в двумерный массив */
    private function resultSetToArray($result_set)
    {
        $array = array();
        while (($row = $result_set->fetch_assoc()) != false) {
            $array[] = (object)$row;
        }
        return $array;
    }

    /* При уничтожении объекта закрывается соединение с базой данных */
    public function __destruct()
    {
        if ($this->mysqli) $this->mysqli->close();
    }
}