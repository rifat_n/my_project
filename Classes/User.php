<?php

require_once('UserStore.php');

class User
{
    private $user_store;

    public function __construct(DataBase $db)
    {
        $this->user_store = new UserStore($db);
    }

    public function getUserData($id)
    {
        return (object)$this->user_store->getUserData($id);
    }

    public function checkErrors($request)
    {
        return $this->user_store->checkErrors($request);
    }

    public function doRegister($request)
    {
        return $this->user_store->doRegister($request);
    }

    public function setSession($id)
    {
        $this->user_store->setSession($id);
    }

    public function checkAuth($request)
    {
        return $this->user_store->checkAuth($request);
    }
}