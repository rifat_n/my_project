<?php

require $_SERVER['DOCUMENT_ROOT'] . '/Classes/includeClasses.php';
require $_SERVER['DOCUMENT_ROOT'] . '/system/config.php';

$db = DataBase::getDB();
$user = new User($db);
$author = new Author($db);
$news = new News($db);

session_start();
$isAuth = isset($_SESSION['user_id']) ? true : false;

if ($isAuth) {
    $userData = $user->getUserData($_SESSION['user_id']);
}