<?php
include_once 'header.php';?>

<div class="main container">
    <?php if ($isAuth):?>
        <p>Добро пожаловать, <?=$userData->login?></p>
    <?php else:?>
        <p>Вы не авторизованы!</p>
        <p>
            Для добавления статей необходимо
            <a href="/auth">авторизоваться</a>
            или
            <a href="/reg">зарегистрироваться</a>
            ...
        </p>
    <?php endif;?>
</div>

<?php include_once 'footer.php';