<?php

include_once '../header.php';

$added = false;
if ($_POST) {
    $newId = $news->insertNew($_POST, $userData->id);
    if ($newId > 0) {
        $added = true;
    }
}

if (!isset($_SESSION['user_id'])) {
    header("Location: /");
    exit();
}?>

<?php if (!$added):?>
    <div class="main container">
        <form action="/news/add.php" method="POST" class="adding">
            <div class="adding__title">
                Добавление новости
            </div>
            <input type="text" name="title" required>
            <textarea name="text" id="addingText" cols="30" rows="10"></textarea>
            <input type="submit" value="Опубликрвать статью">
        </form>
    </div>
<?php else:
    header("Location: /news/");
    exit();
endif;
include_once '../footer.php';