<?php

include_once '../header.php';

if (!$_GET['id']) {
    header("Location: /news/");
    exit();
}

$postData = $news->getPost($_GET['id']);
$authorData = $author->getUserData($postData->user_id);
?>

<div class="main container">
    <div class="post">
        <div class="post__right">
            <?=$authorData->login?>
        </div>
        <div class="post__right">
            <?=$postData->created_at?>
        </div>
        <div class="post__title">
            <?=$postData->title?>
        </div>
        <div class="post__text">
            <?=$postData->post?>
        </div>
    </div>
</div>

<?php include_once '../footer.php';