<?php

include_once '../header.php';

$page = $_GET['p'] ?? 1;
$newsData = $news->getNews($page);
?>

<div class="main container">
    <?php if ($isAuth):?>
        <div class="news__add-block">
            <a href="/news/add.php">Добавить новость</a>
        </div>
    <?php endif;?>

    <div class="news">
        <?php if (isset($newsData['posts'][0])):
            foreach ($newsData['posts'] as $new):
                $authorData = $author->getUserData($new->user_id);?>
                <a href="/news/detail.php?id=<?=$new->id?>" class="new">
                    <span class="new__created-at">
                        Автор: <?=$authorData->login?>
                    </span>
                    <span class="new__created-at">
                        Дата размещения: <?=$new->created_at?>
                    </span>
                    <span class="new__title">
                        <?=$new->title?>
                    </span>
                    <span class="new__text">
                        <?=$new->post?>
                    </span>
                </a>
            <?php endforeach;
        else:?>
            <div class="new">
                Список новостей пуст...
            </div>
        <?php endif;?>
    </div>
    <div class="pagination">
        <?php for ($i = 1; $i <= $newsData['page_count']; $i++):?>
            <a href="/news/?p=<?=$i?>" class="pagination"><?=$i?></a>
        <?php endfor;?>
    </div>
</div>

<?php include_once '../footer.php';