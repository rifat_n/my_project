"use strict";

$(function () {
    let util = new Util();
    window.util = util;
});

function Util() {
    $(document).on('click', '.logout', this.logout)
}

Util.prototype = {
    logout: function () {
        $.ajax({
            type: "POST",
            url: '/ajax/functions.php',
            data: {
                action: 'logout'
            }
        }).then(res => {
            window.location.href = '/';
        });
    }
}