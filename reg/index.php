<?php
include_once '../header.php';

if (isset($_SESSION['user_id'])) {
    header("Location: /");
    exit();
}

$regErrors = [];
$registered = false;

if ($_POST) {
    $regErrors = $user->checkErrors($_POST);
    if (count($regErrors) == 0) {
        $newUser = $user->doRegister($_POST);
        if ($newUser > 0) {
            $registered = true;
        }
    }
}

if (!$registered) :
?>
    <div class="main container">
        <form class="reg__form" method="POST" action="/reg/">
            <input type="text" required placeholder="Имя" name="name" value="<?=$_POST['name'] ?? ''?>">
            <input type="email" required placeholder="email" name="email" value="<?=$_POST['email'] ?? ''?>">
            <input type="password" required placeholder="Пароль" name="pass">
            <input type="password" required placeholder="Повторите пароль" name="pass2">
            <input type="submit" value="Зарегистрироваться">
            <div class="errors">
                <?php foreach ($regErrors as $error):?>
                    <div class="error"><?=$error?></div>
                <?php endforeach;?>
            </div>
        </form>
    </div>
<?php else:
    $session = $user->setSession($newUser);
    header("Location: /");
    exit();
endif;

include_once '../footer.php';